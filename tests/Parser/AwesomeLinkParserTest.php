<?php declare(strict_types=1);

namespace App\Tests\Parser;

use App\Parser\AwesomeLinkParser;
use PHPUnit\Framework\TestCase;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;

class AwesomeLinkParserTest extends TestCase
{
    /**
     * @var AwesomeLinkParser
     */
    private $parser;

    protected function setUp()
    {
        $this->parser = new AwesomeLinkParser(
            new DocParser(
                Environment::createCommonMarkEnvironment()
            )
        );
    }

    public function testAwesomeParsing(): void
    {
        $input = <<<INPUT
- [Node.js](https://github.com/sindresorhus/awesome-nodejs#readme) - JavaScript runtime built on Chrome's V8 JavaScript engine.
- [Frontend Development](https://github.com/dypsilon/frontend-dev-bookmarks#readme)
- [iOS](https://github.com/vsouza/awesome-ios#readme) - Mobile operating system for Apple phones and tablets.
- [Android](https://github.com/JStumpp/awesome-android#readme)
- [IoT & Hybrid Apps](https://github.com/weblancaster/awesome-IoT-hybrid#readme)
- [Electron](https://github.com/sindresorhus/awesome-electron#readme) - Cross-platform native desktop apps using JavaScript/HTML/CSS.
INPUT;

        $expected = [
            'Node.js'              => 'https://github.com/sindresorhus/awesome-nodejs#readme',
            'Frontend Development' => 'https://github.com/dypsilon/frontend-dev-bookmarks#readme',
            'iOS'                  => 'https://github.com/vsouza/awesome-ios#readme',
            'Android'              => 'https://github.com/JStumpp/awesome-android#readme',
            'IoT & Hybrid Apps'    => 'https://github.com/weblancaster/awesome-IoT-hybrid#readme',
            'Electron'             => 'https://github.com/sindresorhus/awesome-electron#readme',
        ];

        $results = $this->parser->parseLinks($input);

        foreach ($expected as $key => $value) {
            $this->assertSame($key, $results->key());
            $this->assertSame($value, $results->current());

            $results->next();
        }
    }
}
