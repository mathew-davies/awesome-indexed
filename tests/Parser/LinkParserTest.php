<?php declare(strict_types=1);

namespace App\Tests\Parser;

use App\Parser\LinkParser;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use PHPUnit\Framework\TestCase;

class LinkParserTest extends TestCase
{
    /**
     * @var LinkParser
     */
    private $parser;

    protected function setUp()
    {
        $this->parser = new LinkParser(
            new DocParser(
                Environment::createCommonMarkEnvironment()
            )
        );
    }

    public function providerTestLinkParsing(): \Generator
    {
        yield [
            '* [Google.com](https://google.com) - Hello this is Google',
            'Google.com',
            'https://google.com',
            'Hello this is Google.',
        ];

        yield [
            '- [Satisfy](https://github.com/ludofleury/satisfy) - Satis composer repository manager with a Web UI.',
            'Satisfy',
            'https://github.com/ludofleury/satisfy',
            'Satis composer repository manager with a Web UI.',
        ];

        yield [
            '- [ToranProxy](https://toranproxy.com/) (deprecated) - In addition to providing a composer repository ToranProxy acts as a proxy server for Packagist and GitHub.',
            'ToranProxy',
            'https://toranproxy.com/',
            '(deprecated) - In addition to providing a composer repository ToranProxy acts as a proxy server for Packagist and GitHub.',
        ];

        yield [
            '* [ElasticSearch PHP](https://github.com/elastic/elasticsearch-php) - The official client library for [ElasticSearch](https://www.elastic.co/).',
            'ElasticSearch PHP',
            'https://github.com/elastic/elasticsearch-php',
            'The official client library for [ElasticSearch](https://www.elastic.co/).'
        ];

        yield [
            '* [php-bench](https://github.com/jacobbednarz/php-bench) - Benchmark and profile PHP code blocks whilst measuring the performance footprint.',
            'php-bench',
            'https://github.com/jacobbednarz/php-bench',
            'Benchmark and profile PHP code blocks whilst measuring the performance footprint.'
        ];

        yield [
            '- [cat-ascii-faces](https://github.com/melaniecebula/cat-ascii-faces) - `₍˄·͈༝·͈˄₎◞ ̑̑ෆ⃛ (=ↀωↀ=)✧ (^･o･^)ﾉ”`.',
            'cat-ascii-faces',
            'https://github.com/melaniecebula/cat-ascii-faces',
            '₍˄·͈༝·͈˄₎◞ ̑̑ෆ⃛ (=ↀωↀ=)✧ (^･o･^)ﾉ”.'
        ];

        yield [
            '* [insight.sensiolabs.com](https://insight.sensiolabs.com/) — Code Quality for PHP/Symfony projects, free for Open Source',
            'insight.sensiolabs.com',
            'https://insight.sensiolabs.com/',
            'Code Quality for PHP/Symfony projects, free for Open Source.'
        ];

        yield [
            '- [Mastodon](https://joinmastodon.org/) | [![github](https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/svg/mark-github.svg) ](https://github.com/tootsuite/mastodon) - Your self-hosted, globally interconnected microblogging community.',
            'Mastodon',
            'https://joinmastodon.org/',
            'Your self-hosted, globally interconnected microblogging community.'
        ];

        yield [
            '* [Mastodon](https://joinmastodon.org/)',
            'Mastodon',
            'https://joinmastodon.org/',
            null
        ];

        yield [
            '* [GraphQL Symfony Bundle](https://github.com/Youshido/GraphQLBundle) – GraphQL Bundle for the Symfony 3 (supports 2.6+).',
            'GraphQL Symfony Bundle',
            'https://github.com/Youshido/GraphQLBundle',
            'GraphQL Bundle for the Symfony 3 (supports 2.6+).'
        ];
    }

    /**
     * @dataProvider providerTestLinkParsing
     *
     * @param string      $input
     * @param string      $name
     * @param string      $url
     * @param string|null $description
     */
    public function testLinkParsing(string $input, string $name, string $url, string $description = null): void
    {
        $result = $this->parser->parseLinks($input);

        $this->assertSame($name, $result->key());
        $this->assertSame($url, $result->current()['url']);
        $this->assertSame($description, $result->current()['description']);
    }
}
