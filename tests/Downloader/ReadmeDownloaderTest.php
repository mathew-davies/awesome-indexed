<?php declare(strict_types=1);

namespace App\Tests\Downloader;

use App\Downloader\ReadmeDownloader;
use App\Downloader\ReadmeDownloaderInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Response;

class ReadmeDownloaderTest extends TestCase
{
    /**
     * @var ReadmeDownloaderInterface
     */
    private $downloader;

    /**
     * @var Client|MockObject
     */
    private $http;

    protected function setUp()
    {
        $this->http = $this->getMockForAbstractClass(
            Client::class, [],
            '',
            true,
            true,
            true,
            ['request', 'getResponse']
        );

        $this->downloader = new ReadmeDownloader(
            $this->http
        );
    }

    public function providerDownload(): \Generator
    {
        yield [
            'https://github.com/sindresorhus/awesome-nodejs#readme',
            'https://raw.githubusercontent.com/sindresorhus/awesome-nodejs/master/readme.md',
        ];
    }

    /**
     * @dataProvider providerDownload
     *
     * @param string $input
     * @param string $expected
     */
    public function testDownload(string $input, string $expected): void
    {
        $this->http->method('request')
            ->with('GET', $expected);

        $this->http->method('getResponse')
            ->willReturn(new Response('content'));

        $result = $this->downloader->download($input);

        $this->assertSame('content', $result);
    }

}
