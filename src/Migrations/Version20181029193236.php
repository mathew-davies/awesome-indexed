<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181029193236 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->platform->getName() !== 'sqlite', 'This platform only supports running on sqlite.');

        $this->addSql('CREATE VIRTUAL TABLE awesome USING fts5(name, url UNINDEXED, link_url UNINDEXED, link_name, link_description);');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE awesome;');
    }
}
