<?php declare(strict_types=1);

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    /**
     * @Route("/", name="search", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        return $this->render('search.html.twig', [
            'searchTerm' => $request->get('search'),
        ]);
    }

    /**
     * Search the dataset and return as JSON.
     *
     * @Route("/results", name="search_results", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function search(Request $request): Response
    {
        $search = $request->get('search');

        $results = $this->get('doctrine')
            ->getRepository('App:Awesome')
            ->search($search);

        return new JsonResponse($results);
    }
}
