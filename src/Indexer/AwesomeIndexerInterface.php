<?php declare(strict_types=1);

namespace App\Indexer;

/**
 * Interface AwesomeIndexerInterface
 *
 * @package App\Indexer
 */
interface AwesomeIndexerInterface
{
    /**
     * Indexes the content into a storage layer.
     *
     * @param string $name
     * @param string $url
     * @param string $linkUrl
     * @param string $linkName
     * @param string $linkDescription
     */
    public function index(string $name, string $url, string $linkUrl = null, string $linkName = null, string $linkDescription = null): void;
}
