<?php declare(strict_types=1);

namespace App\Indexer;

use App\Event\IndexedEvent;
use App\Event\IndexFailedEvent;
use App\Repository\AwesomeRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AwesomeIndexer
 *
 * @package App\Indexer
 */
class AwesomeIndexer implements AwesomeIndexerInterface
{
    /**
     * @var AwesomeRepository
     */
    private $awesomeRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AwesomeIndexer constructor.
     *
     * @param AwesomeRepository        $awesomeRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(AwesomeRepository $awesomeRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->awesomeRepository = $awesomeRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Indexes the content into a storage layer.
     *
     * @param string      $name
     * @param string      $url
     * @param string|null $linkUrl
     * @param string|null $linkName
     * @param string|null $linkDescription
     */
    public function index(string $name, string $url, string $linkUrl = null, string $linkName = null, string $linkDescription = null): void
    {
        $this->eventDispatcher->dispatch(IndexedEvent::NAME,
            new IndexedEvent($name, $url, $linkUrl, $linkName, $linkDescription)
        );

        try {
            $this->awesomeRepository->storeAwesomeList($name, $url, $linkUrl, $linkName, $linkDescription);
        } catch (DBALException $e) {
            $this->eventDispatcher->dispatch(IndexFailedEvent::NAME,
                new IndexFailedEvent($name, $url, $linkName, $linkUrl, $linkDescription, $e)
            );
        }
    }
}
