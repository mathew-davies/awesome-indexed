<?php declare(strict_types=1);

namespace App\EventListener;

use App\Event\DownloadedAwesomeListEvent;
use App\Indexer\AwesomeIndexerInterface;

class DownloadedAwesomeListListener
{
    /**
     * @var AwesomeIndexerInterface
     */
    private $indexer;

    /**
     * DownloadedAwesomeListListener constructor.
     *
     * @param AwesomeIndexerInterface $indexer
     */
    public function __construct(AwesomeIndexerInterface $indexer)
    {
        $this->indexer = $indexer;
    }

    /**
     * @param DownloadedAwesomeListEvent $event
     */
    public function onDownloadedAwesomeList(DownloadedAwesomeListEvent $event): void
    {
        $this->indexer->index(
            $event->getName(),
            $event->getUrl(),
            $event->getLinkUrl(),
            $event->getLinkName(),
            $event->getLinkDescription()
        );
    }
}
