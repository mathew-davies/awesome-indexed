<?php declare(strict_types=1);

namespace App\EventListener;

use App\Downloader\ReadmeDownloaderInterface;
use App\Event\DownloadAwesomeListEvent;
use App\Event\DownloadedAwesomeListEvent;
use App\Parser\LinkParserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DownloadAwesomeListListener
{
    /**
     * @var LinkParserInterface
     */
    private $linkParser;

    /**
     * @var ReadmeDownloaderInterface
     */
    private $readmeDownloader;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * DownloadAwesomeListListener constructor.
     *
     * @param LinkParserInterface $linkParser
     * @param ReadmeDownloaderInterface $readmeDownloader
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        LinkParserInterface $linkParser,
        ReadmeDownloaderInterface $readmeDownloader,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->linkParser = $linkParser;
        $this->readmeDownloader = $readmeDownloader;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onDownloadAwesomeList(DownloadAwesomeListEvent $event): void
    {
        $links = $this->linkParser->parseLinks(
            $this->readmeDownloader->download($event->getUrl())
        );

        foreach ($links as $link) {
            $this->eventDispatcher->dispatch(
                DownloadedAwesomeListEvent::NAME,
                new DownloadedAwesomeListEvent(
                    $event->getName(),
                    $event->getUrl(),
                    $link['url'],
                    $links->key(),
                    $link['description']
                )
            );
        }
    }
}
