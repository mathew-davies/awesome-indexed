<?php declare(strict_types=1);

namespace App\Downloader;

use Symfony\Component\BrowserKit\Client;

class ReadmeDownloader implements ReadmeDownloaderInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * ReadmeDownloader constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     *
     * @return string|null
     */
    public function download(string $url): ?string
    {
        $parsedUrl = parse_url($url);

        $rawUrl = $parsedUrl['scheme'] . '://raw.githubusercontent.com' . $parsedUrl['path'] . '/master';

        $readmeFiles = ['readme.md', 'README.md', 'README.MD', 'Readme.md'];

        foreach ($readmeFiles as $readmeFile) {

            // Attempt to download the raw README file.
            $this->client->request('GET', $rawUrl . '/' . $readmeFile);

            $response = $this->client->getResponse();

            if ($response !== null && $response->getStatus() === 200) {
                return $response->getContent();
            }
        }

        return null;
    }
}
