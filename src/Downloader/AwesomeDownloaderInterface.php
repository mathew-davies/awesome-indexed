<?php declare(strict_types=1);

namespace App\Downloader;

/**
 * Interface AwesomeDownloaderInterface
 *
 * @package App\Downloader
 */
interface AwesomeDownloaderInterface
{
    /**
     * Start the downloading process for a root awesome awesome list.
     *
     * Returns an array where the key is the awesome list name the value
     * is the markdown content.
     *
     * @param string $url
     *
     * @return void
     */
    public function download(string $url): void;
}
