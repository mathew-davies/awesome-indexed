<?php declare(strict_types=1);

namespace App\Downloader;

interface ReadmeDownloaderInterface
{
    /**
     * @param string $url
     *
     * @return string|null
     */
    public function download(string $url): ?string;
}
