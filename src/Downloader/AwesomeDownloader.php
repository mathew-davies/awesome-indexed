<?php declare(strict_types=1);

namespace App\Downloader;

use App\Event\DownloadAwesomeListEvent;
use App\Parser\LinkParserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AwesomeDownloader
{
    /**
     * @var ReadmeDownloaderInterface
     */
    private $readmeDownloader;

    /**
     * @var LinkParserInterface
     */
    private $awesomeLinkParser;

    /**
     * @var EventDispatcherInterface
     */
    private $eventListener;

    /**
     * AwesomeDownloader constructor.
     *
     * @param ReadmeDownloaderInterface $readmeDownloader
     * @param LinkParserInterface       $awesomeLinkParser
     * @param EventDispatcherInterface  $eventListener
     */
    public function __construct(
        ReadmeDownloaderInterface $readmeDownloader,
        LinkParserInterface $awesomeLinkParser,
        EventDispatcherInterface $eventListener
    ) {
        $this->readmeDownloader = $readmeDownloader;
        $this->awesomeLinkParser = $awesomeLinkParser;
        $this->eventListener = $eventListener;
    }

    public function crawl(): void
    {
        $awesomeLists = $this->awesomeLinkParser->parseLinks(
            $this->readmeDownloader->download('https://github.com/sindresorhus/awesome')
        );

        foreach ($awesomeLists as $name => $url) {
            $this->eventListener->dispatch(DownloadAwesomeListEvent::NAME, new DownloadAwesomeListEvent($name, $url));
        }
    }
}
