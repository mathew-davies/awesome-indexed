<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Awesome;
use App\Parser\AwesomeLinkParser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AwesomeRepository
 *
 * @package App\Repository
 */
class AwesomeRepository extends ServiceEntityRepository
{
    /**
     * AwesomeRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Awesome::class);
    }

    /**
     * Stores the awesome list in the database.
     *
     * @param string $name
     * @param string $url
     * @param string $linkUrl
     * @param string $linkName
     * @param string $linkDescription
     *
     * @throws DBALException
     */
    public function storeAwesomeList(
        string $name,
        string $url,
        string $linkUrl = null,
        string $linkName = null,
        string $linkDescription = null
    ): void {
        $connection = $this->getEntityManager()->getConnection();

        $connection->insert('awesome', [
            'name'             => $name,
            'url'              => $url,
            'link_url'         => $linkUrl,
            'link_name'        => $linkName,
            'link_description' => $linkDescription,
        ]);
    }

    /**
     * Searches for matching entries using MySQL fulltext.
     *
     * @param string $term
     *
     * @return Awesome[]
     */
    public function search(string $term): array
    {
        return $this->getEntityManager()->getConnection()->fetchAll(
            '
            SELECT name, url, link_url, link_name, link_description
            FROM awesome 
            WHERE awesome MATCH ?
            ORDER BY rank',
            ['link_description:' . $term . ' OR link_name:' . $term]
        );
    }
}
