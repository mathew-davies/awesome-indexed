<?php declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class IndexedEvent
 *
 * @package App\Event
 */
class IndexedEvent extends Event
{
    public const NAME = 'indexed';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string|null
     */
    private $linkUrl;

    /**
     * @var string|null
     */
    private $linkName;

    /**
     * @var string|null
     */
    private $linkDescription;

    /**
     * IndexedEvent constructor.
     *
     * @param string $name
     * @param string $url
     * @param null|string $linkUrl
     * @param null|string $linkName
     * @param null|string $linkDescription
     */
    public function __construct(
        string $name,
        string $url,
        ?string $linkUrl,
        ?string $linkName,
        ?string $linkDescription
    ) {
        $this->name = $name;
        $this->url = $url;
        $this->linkUrl = $linkUrl;
        $this->linkName = $linkName;
        $this->linkDescription = $linkDescription;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return null|string
     */
    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    /**
     * @return null|string
     */
    public function getLinkName(): ?string
    {
        return $this->linkName;
    }

    /**
     * @return null|string
     */
    public function getLinkDescription(): ?string
    {
        return $this->linkDescription;
    }
}
