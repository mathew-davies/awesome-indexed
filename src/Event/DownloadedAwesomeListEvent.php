<?php declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class DownloadedAwesomeListEvent
 *
 * @package App\Event
 */
class DownloadedAwesomeListEvent extends Event
{
    public const NAME = 'downloaded_awesome_list';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $linkUrl;

    /**
     * @var string
     */
    private $linkName;

    /**
     * @var string
     */
    private $linkDescription;

    /**
     * DownloadedAwesomeListEvent constructor.
     *
     * @param string $name
     * @param string $url
     * @param string $linkUrl
     * @param string $linkName
     * @param string $linkDescription
     */
    public function __construct(string $name, string $url, string $linkUrl = null, string $linkName = null, string $linkDescription = null)
    {
        $this->name = $name;
        $this->url = $url;
        $this->linkUrl = $linkUrl;
        $this->linkName = $linkName;
        $this->linkDescription = $linkDescription;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    /**
     * @return string
     */
    public function getLinkName(): ?string
    {
        return $this->linkName;
    }

    /**
     * @return string
     */
    public function getLinkDescription(): ?string
    {
        return $this->linkDescription;
    }
}
