<?php declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

class DownloadAwesomeListEvent extends Event
{
    public const NAME = 'download_awesome_list';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * DownloadAwesomeListEvent constructor.
     *
     * @param string $name
     * @param string $url
     */
    public function __construct(string $name, string $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
