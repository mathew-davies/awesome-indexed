<?php declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class DownloadFailedEvent
 *
 * @package App\Event
 */
class DownloadFailedEvent extends Event
{
    public const NAME = 'download.failed';

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * DownloadedAwesomeListEvent constructor.
     *
     * @param string     $name
     * @param \Exception $exception
     */
    public function __construct(string $name, \Exception $exception)
    {
        $this->name = $name;
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return \Exception
     */
    public function getException(): \Exception
    {
        return $this->exception;
    }
}
