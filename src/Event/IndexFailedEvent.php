<?php declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class IndexFailedEvent
 *
 * @package App\Event
 */
class IndexFailedEvent extends Event
{
    public const NAME = 'index.failed';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string|null
     */
    private $linkUrl;

    /**
     * @var string|null
     */
    private $linkName;

    /**
     * @var string|null
     */
    private $linkDescription;

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * IndexFailedEvent constructor.
     *
     * @param string $name
     * @param string $url
     * @param null|string $linkUrl
     * @param null|string $linkName
     * @param null|string $linkDescription
     * @param \Exception $exception
     */
    public function __construct(
        string $name,
        string $url,
        ?string $linkUrl,
        ?string $linkName,
        ?string $linkDescription,
        \Exception $exception
    ) {
        $this->name = $name;
        $this->url = $url;
        $this->linkUrl = $linkUrl;
        $this->linkName = $linkName;
        $this->linkDescription = $linkDescription;
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return null|string
     */
    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    /**
     * @return null|string
     */
    public function getLinkName(): ?string
    {
        return $this->linkName;
    }

    /**
     * @return null|string
     */
    public function getLinkDescription(): ?string
    {
        return $this->linkDescription;
    }

    /**
     * @return \Exception
     */
    public function getException(): \Exception
    {
        return $this->exception;
    }
}
