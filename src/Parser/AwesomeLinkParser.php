<?php declare(strict_types=1);

namespace App\Parser;

use League\CommonMark\DocParser;
use League\CommonMark\Inline\Element\Link;
use League\CommonMark\Inline\Element\Text;

/**
 * Class AwesomeLinkParser
 *
 * @package App\Parser
 */
class AwesomeLinkParser implements LinkParserInterface
{
    /**
     * @var DocParser
     */
    private $parser;

    /**
     * AwesomeLinkParser constructor.
     *
     * @param DocParser $parser
     */
    public function __construct(DocParser $parser)
    {
        $this->parser = $parser;
    }

    public function parseLinks(string $markdown = null): \Generator
    {
        if ($markdown === null) {
            return [];
        }

        $document = $this->parser->parse($markdown);
        $walker = $document->walker();

        while ($event = $walker->next()) {
            $node = $event->getNode();

            if (!$node instanceof Link || !$event->isEntering() || !$this->isAwesome($node)) {
                continue;
            }

            $firstChild = $node->firstChild();

            if ($firstChild instanceof Text) {
                yield $firstChild->getContent() => $node->getUrl();
            }
        }
    }

    /**
     * Tells us if the link is an awesome link, or not ...
     *
     * @param Link $link
     *
     * @return bool
     */
    private function isAwesome(Link $link): bool
    {
        return strpos($link->getUrl(), 'https://github.com') === 0;
    }
}
