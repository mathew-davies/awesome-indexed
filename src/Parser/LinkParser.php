<?php declare(strict_types=1);

namespace App\Parser;

use League\CommonMark\DocParser;
use League\CommonMark\Node\Node;
use function Stringy\create as s;
use League\CommonMark\Inline\Element\Link;
use League\CommonMark\Inline\Element\Text;
use League\CommonMark\Block\Element\ListItem;
use League\CommonMark\Block\Element\Paragraph;
use League\CommonMark\Inline\Element\AbstractWebResource;
use League\CommonMark\Inline\Element\AbstractStringContainer;

class LinkParser implements LinkParserInterface
{
    /**
     * @var DocParser
     */
    private $parser;

    /**
     * AwesomeLinkParser constructor.
     *
     * @param DocParser $parser
     */
    public function __construct(DocParser $parser)
    {
        $this->parser = $parser;
    }

    public function parseLinks(string $markdown = null): \Generator
    {
        if ($markdown === null) {
            return [];
        }

        $document = $this->parser->parse($markdown);
        $walker = $document->walker();

        while ($event = $walker->next()) {
            if (!$event->isEntering()) {
                continue;
            }

            $node = $event->getNode();

            $listItem = $this->getListItem($node);

            if (!$listItem) {
                continue;
            }

            $link = $this->getLink($listItem);

            if (!$link) {
                continue;
            }

            $description = $this->getDescription($listItem);
            $linkText = $this->getLinkText($link);

            yield $linkText => [
                'url'         => $link->getUrl(),
                'description' => $description
            ];
        }
    }

    private function getListItem(Node $node): ?Paragraph
    {
        $firstChild = $node->firstChild();

        if ($node instanceof ListItem && $firstChild instanceof Paragraph) {
            return $firstChild;
        }

        return null;
    }

    private function getLink(Paragraph $node): ?Link
    {
        $firstChild = $node->firstChild();

        if ($firstChild instanceof Link) {
            return strpos($firstChild->getUrl(), '#') !== 0
                ? $firstChild
                : null;
        }

        return null;
    }

    private function getLinkText(Link $node): ?string
    {
        $firstChild = $node->firstChild();

        if ($firstChild instanceof Text) {
            return $firstChild->getContent();
        }

        return null;
    }

    private function getDescription(Paragraph $node): ?string
    {
        $content = '';

        $firstChild = $node->firstChild();

        if ($firstChild instanceof Link) {
            $nextNode = $firstChild->next();

            while ($nextNode) {
                if ($nextNode instanceof AbstractStringContainer) {
                    $content .= $nextNode->getContent();
                } elseif ($nextNode instanceof AbstractWebResource) {
                    $webResourceFirstChild = $nextNode->firstChild();

                    $content .= $webResourceFirstChild instanceof AbstractStringContainer
                        ? sprintf('[%s](%s)', $webResourceFirstChild->getContent(), $nextNode->getUrl())
                        : '';
                }

                $nextNode = $nextNode->next();
            }

            if (empty($content)) {
                return null;
            }

            return s(s($content)->trim())->trim('.-—–| ') . '.';
        }

        return null;
    }
}
