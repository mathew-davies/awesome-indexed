<?php declare(strict_types=1);

namespace App\Parser;

interface LinkParserInterface
{
    /**
     * @param string|null $markdown
     *
     * @return \Generator
     */
    public function parseLinks(string $markdown = null): \Generator;
}
