<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AwesomeRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @ORM\Table(
 *     options={"collate" = "utf8mb4_unicode_ci", "charset" = "utf8mb4"},
 *     indexes={@ORM\Index(columns={"markdown_content"}, flags={"fulltext"})})
 * )
 */
class Awesome
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="rowid", type="integer")
     */
    private $id;

    /**
     * Awesome list name
     *
     * @ORM\Column(type="string", nullable=false, name="name", unique=true)
     *
     * @var string
     */
    private $name;

    /**
     * Awesome list URL.
     *
     * @ORM\Column(type="string", nullable=false, name="url", unique=true)
     *
     * @var string
     */
    private $url;

    /**
     * Awesome list markdown content
     *
     * @ORM\Column(type="text", nullable=false, name="markdown_content")
     *
     * @var string
     */
    private $markdownContent;

    /**
     * @ORM\Column(type="datetime", nullable=false, name="updated_at", options={"default": "CURRENT_TIMESTAMP"})
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Awesome constructor.
     *
     * @param string $name
     * @param string $url
     * @param string $markdownContent
     */
    public function __construct(string $name, string $url, string $markdownContent)
    {
        $this->name = $name;
        $this->url = $url;
        $this->markdownContent = $markdownContent;
    }

    /**
     * @ORM\PreFlush()
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMarkdownContent(): string
    {
        return $this->markdownContent;
    }

    /**
     * @param string $markdownContent
     */
    public function setMarkdownContent(string $markdownContent): void
    {
        $this->markdownContent = $markdownContent;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * Returns the portion of the text that matches the term.
     *
     * @param string $term
     *
     * @return string
     */
    public function getMatch(string $term): string
    {
        $result = preg_match('/^.*' . $term . '.*$/im', $this->getMarkdownContent(), $matches);

        if (!$result) {
            return '';
        }

        return $matches[0];
    }
}
