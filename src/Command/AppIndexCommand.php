<?php declare(strict_types=1);

namespace App\Command;

use App\Downloader\AwesomeDownloader;
use App\Downloader\AwesomeDownloaderInterface;
use App\Event\DownloadedAwesomeListEvent;
use App\Indexer\AwesomeIndexerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AppIndexCommand
 *
 * @package App\Command
 */
class AppIndexCommand extends Command
{
    protected static $defaultName = 'app:index';

    /**
     * @var AwesomeDownloader
     */
    private $downloader;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AppIndexCommand constructor.
     *
     * @param AwesomeDownloader        $downloader
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(AwesomeDownloader $downloader, EventDispatcherInterface $eventDispatcher)
    {
        $this->downloader = $downloader;
        $this->eventDispatcher = $eventDispatcher;

        parent::__construct();
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this->setDescription('Indexes the awesome list content.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @see setCode()
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% -- %message%');

        $io = new SymfonyStyle($input, $output);

        $io->title('Welcome to the Awesome Indexer v1.0');
        $io->text('This tool will download and index all awesome lists starting');
        $io->text('at the root. When indexing is complete, search with app:search');

        $io->newLine();

        $ready = $io->ask('Ready? [y/n]', 'y', function (string $answer) {
            return $answer === 'y';
        });

        if (!$ready) {
            $io->note('Not ready, aborting.');
            return 1;
        }

        $progressBar = new ProgressBar($output);
        $progressBar->setFormat('custom');

        $io->section('Ready, starting the process');

        $this->eventDispatcher->addListener(DownloadedAwesomeListEvent::NAME,
            function (DownloadedAwesomeListEvent $event) use ($progressBar) {
                $progressBar->setMessage($event->getLinkUrl());
                $progressBar->advance();
            },
            255
        );

        $progressBar->finish();

//        $this->eventDispatcher->addListener(IndexedEvent::NAME,
//            function (IndexedEvent $event) use ($io) {
//                $io->text(' * (indexed) - ' . $event->getName());
//            },
//            255
//        );

        try {
            $this->downloader->crawl();
        } catch (InvalidArgumentException $exception) {
            $io->error($exception->getMessage());
            return 1;
        }

        return 0;
    }
}
