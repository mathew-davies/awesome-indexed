class Backend {
    /**
     * @param {string} endpoint
     */
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Performs a search against the backend.
     *
     * @param {string} search
     *
     * @returns {Promise<Response>}
     */
    search(search) {
        return fetch(this.endpoint + '?' + new URLSearchParams({search: search})).then(response => response.json())
    }
}

class Frontend {
    /**
     * @param {HTMLElement} results
     * @param {HTMLElement} noResults
     * @param {HTMLElement} tutorial
     */
    constructor(results, noResults, tutorial) {
        this.results = results;
        this.noResults = noResults;
        this.tutorial = tutorial;
    }

    /**
     * Clears the results.
     */
    clearResults() {
        this.noResults.style.display = 'none';
        this.results.style.display = 'none';
        this.tutorial.style.display = 'none';
        this.results.innerHTML = '';
    }

    /**
     * Show the tutorial.
     */
    showTutorial() {
        this.tutorial.style.display = 'block';
    }

    /**
     * Renders a list of results.
     *
     * @param {Response} results
     */
    renderResults(results) {
        this.clearResults();

        if (results.length > 0) {
            this.results.style.display = 'block';
        } else {
            this.noResults.style.display = 'block';
        }

        results.forEach(result => {
            this.results.appendChild(
                Frontend.render(result)
            );
        })
    }

    /**
     * Renders a single result.
     *
     * @param {Object} result
     *
     * @returns {HTMLElement}
     */
    static render(result) {
        let container = document.createElement('div');

        container.innerHTML = `
            <div class="b--near-white b--solid bt-0 bl-0 br-0 bw1 hover-bg-near-white">
                <div class="w-80 pa3 cf relative center">
                    <a href="${result.link_url}" class="ml2 link w-70 db fl awesome-purple-color">
                        <h2 class="f4 ma0 mb2">${result.link_name}</h2>
                        <p class="ma0 lh-copy" style="color:#918EA2">${result.link_description ? result.link_description : 'No description.'}</p>
                    </a>
                    <a href="${result.url}" class="bg-light-green absolute right-0 top-0 pa2 mt4 mr4 dib awesome-purple-color fr">${result.name}</a>
                </div>
            </div>
        `;

        return container;
    }
}

class Browser {
    /**
     * @param {History} history
     */
    constructor(history) {
        this.history = history;
    }

    /**
     * Update the URL with the correct query.
     *
     * @param {string} search
     */
    updateUrl(search) {
        const query = new URLSearchParams({
            search: search
        });

        this.history.pushState({}, null, '?' + query);
    }
}

class SearchForm {
    /**
     * @param {Backend} backend
     * @param {Frontend} frontend
     */
    constructor(backend, frontend) {
        this.backend = backend;
        this.frontend = frontend;
    }

    /**
     * Set the value of the search form.
     *
     * @param {string} value
     */
    setValue(value) {
        this.value = value;
    }

    /**
     * Value of the search input.
     *
     * @returns {string}
     */
    getValue() {
        return this.value;
    }

    /**
     * @returns {boolean}
     */
    onSubmit() {
        this.backend
            .search(this.value)
            .then(response => {
                this.frontend.renderResults(response);
            });
    }
}

const frontend = new Frontend(
    document.getElementById('results'),
    document.getElementById('no-results'),
    document.getElementById('tutorial')
);

const browser = new Browser(window.history);
const backend = new Backend('/results');
const searchForm = new SearchForm(backend, frontend);

function onSearch() {
    const value = document.getElementById('search').value;

    if (value) {
        searchForm.setValue(value);
        searchForm.onSubmit();
        browser.updateUrl(value);

        return false;
    }

    frontend.clearResults();
    frontend.showTutorial();

    return false;
}

const params = new URLSearchParams(
    new URL(window.location).search
);

if (params.get('search')) {
    searchForm.setValue(params.get('search'));
    searchForm.onSubmit();
} else {
    frontend.showTutorial();
}
